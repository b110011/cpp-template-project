Version Control System
======================

C++ module to provide information about last commit in the repository, that has
been available during building of this module.

****

.. include:: class_view_hierarchy.rst

.. include:: unabridged_api.rst
