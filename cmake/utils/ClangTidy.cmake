# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

option(ENABLE_CLANG_TIDY "Enable static analysis with clang-tidy during build process" OFF)

option(ENABLE_CLANG_TIDY_REPORT "Enable static analysis with clang-tidy as a separate target" OFF)
mark_as_advanced(ENABLE_CLANG_TIDY_REPORT)

macro(_enable_clangtidy_inbuild)
  set(CMAKE_CXX_CLANG_TIDY ${CLANGTIDY})
endmacro()

# https://pspdfkit.com/blog/2018/using-clang-tidy-and-integrating-it-in-jenkins/
# https://github.com/PSPDFKit-labs/clang-tidy-to-junit
macro(_enable_clangtidy_report)
  set(CLANGTIDY_RESULTS ${CMAKE_BINARY_DIR}/clangtidy_results)
  file(MAKE_DIRECTORY ${CLANGTIDY_RESULTS})

  list(APPEND CLANGTIDY_ARGS
    -p ${CMAKE_BINARY_DIR} ${CMAKE_SOURCE_DIR}/project
  )

  add_custom_target(clang-tidy
    COMMAND
      ${CLANGTIDY} ${CLANGTIDY_ARGS}
    COMMENT
      "Generate clang-tidy report for the project"
  )
  add_custom_target(clang-tidy-report
    COMMAND
      ${CLANGTIDY} ${CLANGTIDY_ARGS} > clangtidy_result.txt
    COMMAND
      cat clangtidy_result.txt | ${CMAKE_SOURCE_DIR}/scripts/clang-tidy-to-junit.py ${CMAKE_SOURCE_DIR}/project > clangtidy_junit.xml
    COMMENT
      "Generate clang-tidy TXT report for the project"
    VERBATIM
    WORKING_DIRECTORY
      ${CLANGTIDY_RESULTS}
  )
endmacro()

find_program(CLANGTIDY
  NAMES
    clang-tidy clang-tidy-10
  DOC
    "Path to clang-tidy executable"
)
if(CLANGTIDY)
  if(ENABLE_CLANG_TIDY)
    _enable_clangtidy_inbuild()
  endif()

  if(ENABLE_CLANG_TIDY_REPORT)
    _enable_clangtidy_report()
  endif()
else()
  message(SEND_ERROR "clang-tidy requested but executable not found")
endif()
